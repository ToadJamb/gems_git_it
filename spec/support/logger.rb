FileUtils.mkdir_p 'log'
FileUtils.touch 'log/test.log'

log_file = File.open('log/test.log', 'a+')
log_file.sync = true

logger = Logger.new(log_file)
logger.level = Logger::DEBUG

GitIt.logger = logger

GitIt.logger.debug '--- START ---------'
