require 'spec_helper'

RSpec.describe GitIt::GitResult do
  shared_examples 'git result' do |cmd, out, err, success, is_err, exitstatus|
    subject { described_class.new cmd, shell_result, exitstatus }

    let(:shell_result) { ExecuteShell.run cmd }

    context "given `#{cmd}` is run" do
      describe '#command' do
        it "returns #{cmd}" do
          expect(subject.command).to eq cmd
        end
      end

      describe '#stdout' do
        if out.is_a?(String)
          it "returns #{out}" do
            expect(subject.stdout).to eq out
          end
        else
          it "matches #{out}" do
            expect(subject.stdout).to match out
          end
        end
      end

      describe '#stderr' do
        if err.is_a?(String)
          it "returns #{err}" do
            expect(subject.stderr).to eq err
          end
        else
          it "matches #{err}" do
            expect(subject.stderr).to match err
          end
        end
      end

      describe '#success?' do
        it "returns #{success.inspect}" do
          expect(subject.success?).to eq success
        end
      end

      describe '#error?' do
        it "returns #{is_err.inspect}" do
          expect(subject.error?).to eq is_err
        end
      end
    end
  end

  it_behaves_like 'git result', 'echo foo', 'foo', nil, true, false, 0
  it_behaves_like 'git result', '>&2 echo foo', nil, 'foo', false, true, 0

  it_behaves_like 'git result', 'echo foo; >&2 echo bar',
    'foo', 'bar', false, true, 0

  it_behaves_like 'git result', 'echo foo', 'foo', nil, false, false, 1
  it_behaves_like 'git result', 'echo foo; >&2 echo bar',
    'foo', 'bar', false, true, 2
end
