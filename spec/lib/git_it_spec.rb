require 'spec_helper'

RSpec.describe GitIt::Git do
  subject { instance }

  let(:instance) { described_class.new dir }

  let(:status) { instance.status }

  let(:dir) { FileUtils.mkdir_p 'tmp'; @dir = Dir.mktmpdir(nil, 'tmp') }

  after { FileUtils.rm_rf dir if defined?(@dir) }

  describe '#git' do
    subject { instance.git }

    it 'returns the help text with no errors' do
      expect(subject).to be_a GitIt::GitResult

      expect(subject.stdout).to match(/usage: git/)

      expect(subject.stderr).to eq nil

      expect(subject.exitstatus).to eq 1
      expect(subject.success?).to eq false
      expect(subject.error?).to eq false
    end
  end

  context 'given no repo' do
    it 'behaves as expected' do
      expect(subject.is_repo?).to eq false
      expect(subject.status).to eq nil
    end
  end

  context 'given an empty repo' do
    before do
      Dir.chdir(dir) do
        `git init .`
        expect($?.exitstatus).to eq 0
      end
    end

    it 'behaves as expected' do
      expect(subject.is_repo?).to eq true

      expect(status).to be_a GitIt::StatusResult
      expect(status.stdout).to eq nil
      expect(status.stderr).to eq nil
      expect(status.success?).to eq true
      expect(status.error?).to eq false
      expect(status.exitstatus).to eq 0

      expect(status.clean?).to eq true
    end
  end

  context 'given a repo with changes' do
    before do
      Dir.chdir(dir) do
        `git init`
        expect($?.exitstatus).to eq 0

        FileUtils.touch 'foo'
        FileUtils.touch 'bar'
        File.write 'baz', 'baz content'
        FileUtils.touch 'fizz'
        FileUtils.touch 'buzz'

        `git add foo bar baz`
        expect($?.exitstatus).to eq 0

        `git commit -m "initial commit"`
        expect($?.exitstatus).to eq 0

        `git add fizz`
        expect($?.exitstatus).to eq 0

        `git mv baz qux`
        expect($?.exitstatus).to eq 0

        File.write 'foo', 'foo content'
        FileUtils.rm 'bar'

        #system 'git status --short'
      end
    end

    it 'behaves as expected' do
      expect(subject.is_repo?).to eq true

      expect(status).to be_a GitIt::StatusResult

      expect(status.stdout.empty?).to eq false
      expect(status.stderr).to eq nil

      expect(status.success?).to eq true
      expect(status.error?).to eq false

      expect(status.exitstatus).to eq 0

      expect(status.files.keys)
        .to match_array ['foo', 'bar', 'qux', 'fizz', 'buzz']

      expect(status.clean?).to eq false

      expect(status.files['foo'].state).to eq :modified
      expect(status.files['bar'].state).to eq :deleted
      expect(status.files['qux'].state).to eq :renamed
      expect(status.files['fizz'].state).to eq :added
      expect(status.files['buzz'].state).to eq :new

      expect(status.files['foo'].modified?).to eq true
      expect(status.files['bar'].deleted?).to eq true
      expect(status.files['qux'].renamed?).to eq true
      expect(status.files['fizz'].added?).to eq true
      expect(status.files['buzz'].new?).to eq true

      expect(status.modified.keys).to eq ['foo']
      expect(status.modified).to eq({'foo' => status.files['foo']})

      expect(status.cached.count).to eq 2
      expect(status.cached.keys).to match_array ['fizz', 'qux']

      add_result = subject.add('foo')
      expect(add_result.success?).to eq true
      expect(subject.status.cached.keys).to match_array ['foo', 'fizz', 'qux']

      add_result = subject.add('.')
      expect(add_result.success?).to eq true

      status = subject.status

      expect(status.new.count).to eq 0

      expect(status.cached.keys).to match_array ['foo', 'buzz', 'fizz', 'qux']
    end
  end
end
