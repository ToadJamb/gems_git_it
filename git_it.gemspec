# coding: utf-8
Gem::Specification.new do |spec|
  spec.name    = 'git_it'
  spec.version = '0.0.0'
  spec.authors = ['Travis Herrick']
  spec.email   = ['tthetoad@gmail.com']

  spec.summary     = %q{Simple Git utilities.}
  spec.description = %q{Simpler than the git gem and cross-platform.}
  spec.homepage    = 'https://www.bitbucket.org/toadjamb/git_it'
  spec.license     = 'MIT'

  spec.cert_chain = ['certs/gem-public_key.pem']

  spec.files = Dir['lib/**/*.rb', 'license/*']

  spec.extra_rdoc_files << 'readme.md'

  spec.require_paths = ['lib']

  spec.add_dependency 'execute_shell', '~> 1.0'

  spec.add_development_dependency 'rake',        '~> 12.0'
  spec.add_development_dependency 'rspec',       '~>  3.0'
  spec.add_development_dependency 'rake_tasks',  '~>  5.1'
  spec.add_development_dependency 'travis-yaml', '~>  0.2'
  spec.add_development_dependency 'wwtd',        '~>  1.3'
  spec.add_development_dependency 'cane',        '~>  3.0'
end
