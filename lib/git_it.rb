require 'logger'
require 'shellwords'

require 'execute_shell'

require_relative 'git_it/status_file'
require_relative 'git_it/git_result'

module GitIt
  extend self

  def logger
    return @logger if defined?(@logger)

    @logger ||= Logger.new(STDOUT)
    @logger.level = Logger::UNKNOWN

    @logger
  end

  def logger=(custom_logger)
    @logger = custom_logger
  end

  class Git
    attr_accessor :path

    class << self
      def add_whitelist(*cmds)
        @whitelist ||= {}

        cmds.each do |cmd|
          @whitelist[cmd] = true
        end
      end

      def add_command(key, *params, class_name: nil)
        @commands ||= {}

        if @commands[key]
          raise "Key #{key.inspect} already exists in #{@commands.keys}"
        end

        @commands[key] = {
          :class  => class_name || GitIt::GitResult,
          :params => ['git'] + params.flatten,
        }
      end

      def run(git, cmd, *args)
        git_cmd = commands[cmd][:params][1]

        if !git.is_repo? && !whitelist[git_cmd] \
            && commands[cmd][:params].count > 1
          return
        end

        args ||= []

        set_command_class_for cmd

        args = args.flatten.map do |arg|
          arg.match(/[ '"]/) ? Shellwords.escape(arg) : arg
        end

        command = (commands[cmd][:params] + args.flatten).join(' ')

        shell_result = ExecuteShell.run command, git.path

        klass = commands[cmd][:class]

        result = klass.new(command, shell_result, $?.exitstatus)

        log_result command, result

        result
      end

      def commands
        @commands ||= {}
      end

      def whitelist
        @whitelist ||= {}
      end

      private

      def log_result(command, result)
        GitIt.logger.info command

        if result.stdout
          GitIt.logger.debug { "out: #{result.stdout.inspect}" }
        end

        if result.stderr || result.exitstatus != 0
          GitIt.logger.warn do
            "%s :: exitstatus: %s :: err: %s" % [
              command,
              result.exitstatus,
              result.stderr.inspect,
            ]
          end
        end
      end

      def set_command_class_for(cmd)
        return if commands[cmd][:class].is_a?(Class)

        class_name = commands[cmd][:class]

        commands[cmd][:class] = const_get(class_name)
      end
    end

    add_whitelist 'init', 'clone'

    add_command :git
    add_command :add, 'add'
    add_command :status, 'status', '--short', '--porcelain',
      :class_name => GitIt::StatusResult.to_s
    add_command :push, 'push'
    add_command :push_origin_head, 'push', 'origin', 'HEAD'
    add_command :commit, 'commit', '--message'

    def initialize(path)
      @path = File.expand_path(path)
    end

    def is_repo?
      git_dir = File.join(@path, '.git')
      !!File.directory?(git_dir)
    end

    def run(method, *args)
      self.class.run self, method, *args
    end

    private

    def method_missing(method, *args, &block)
      if self.class.commands[method]
        self.run(method, *args)
      else
        super
      end
    end
  end
end
