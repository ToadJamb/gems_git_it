module GitIt
  class StatusFile
    attr_reader(*[
      :path,
      :state,
    ])

    class << self
      def status_map
        @status_map ||= {
          'M'  => :modified,
          'A'  => :added,
          'D'  => :deleted,
          'R'  => :renamed,
          '??' => :new,
        }
      end
    end

    status_map.values.each do |status|
      define_method "#{status}?" do
        state == status
      end
    end

    def initialize(line)
      @path   = parse_path_from(line)
      @cached = !!line.match(/^\w  /)
      @state  = parse_state_from(line)
    end

    def cached?
      @cached
    end

    private

    def parse_path_from(line)
      file = line[2..-1].strip
      file.match(/->/) ? file.match(/->(.*)/)[1].to_s.strip : file
    end

    def parse_state_from(line)
      key = line[0..1].strip
      status_map[key]
    end

    def status_map
      self.class.status_map
    end
  end
end
