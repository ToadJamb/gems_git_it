module GitIt
  class GitResult
    attr_reader :command, :stdout, :stderr, :exitstatus

    def initialize(command, shell, exitstatus)
      @exitstatus = exitstatus

      @command = command

      @stdout = shell.out.empty? ? nil : shell.out
      @stderr = shell.err.empty? ? nil : shell.err

      on_init if respond_to?(:on_init)
    end

    def success?
      @exitstatus == 0 && !@stderr
    end

    def error?
      !!@stderr
    end
  end

  class StatusResult < GitResult
    attr_accessor :files

    def on_init
      @files = {}

      (@stdout || '').split("\n").each do |file|
        status_file = StatusFile.new(file)
        @files[status_file.path] = status_file
      end
    end

    def clean?
      !@files.any?
    end

    def cached
      @files.select { |k, v| v.cached? }
    end

    StatusFile.status_map.values.each do |status|
      define_method "#{status}" do
        @files.select { |k, v| v.send "#{status}?" }
      end
    end
  end
end
